package main

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"integration-test/cache"
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	_ "github.com/testcontainers/testcontainers-go"
)

type TestDataStoreSuite struct {
	suite.Suite
	store          DataStore
	storeContainer testcontainers.Container
}

var (
	redisAddr     string
	redisPassword = ""
	redisDB       = 0
)

func (suite *TestDataStoreSuite) SetupSuite() {
	ctx := context.Background()
	req := testcontainers.ContainerRequest{
		Image:        "redis:7",
		ExposedPorts: []string{"6379/tcp"},
		WaitingFor:   wait.ForLog("Ready to accept connections"),
	}
	var err error
	suite.storeContainer, err = testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		log.Fatalf("Could not start redis: %s", err)
	}

	redisAddr, err = suite.storeContainer.Endpoint(ctx, "")
	suite.store = cache.NewRedisDataStore(redisAddr, redisPassword, redisDB)

	fmt.Println("SetupSuite called")
}

func (suite *TestDataStoreSuite) TearDownSuite() {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*20)
	if err := suite.storeContainer.Terminate(ctx); err != nil {
		log.Fatalf("Could not stop redis: %s", err)
	}

	fmt.Println("TearDownSuite called")
}

func (suite *TestDataStoreSuite) TestDataStore_Get() {
	val, err := suite.store.Get("non_existing_key")
	assert.NotNil(suite.T(), err)
	assert.Nil(suite.T(), val)
	assert.Empty(suite.T(), val)
}

func (suite *TestDataStoreSuite) TestDataStore_GetAfterSet() {
	err := suite.store.Set("test_key2", "test_value")
	assert.Nil(suite.T(), err)
	val, err := suite.store.Get("test_key2")
	assert.Equal(suite.T(), val, "test_value")
	assert.Nil(suite.T(), err)

}

func (suite *TestDataStoreSuite) TestDataStore_Set() {
	err := suite.store.Set("test_key", "test_value")
	assert.Nil(suite.T(), err)
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(TestDataStoreSuite))
}
