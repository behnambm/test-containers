package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
)

// RedisDataStore is an implementation of the DataStore interface using Redis
type RedisDataStore struct {
	client *redis.Client
}

// NewRedisDataStore creates a new instance of RedisDataStore
func NewRedisDataStore(addr, password string, db int) *RedisDataStore {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})

	return &RedisDataStore{client: client}
}

// Get retrieves the value associated with the given key from Redis
func (r *RedisDataStore) Get(key string) (interface{}, error) {
	val, err := r.client.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return nil, fmt.Errorf("key not found: %s", key)
	} else if err != nil {
		return nil, err
	}

	return val, nil
}

// Set sets the value associated with the given key in Redis
func (r *RedisDataStore) Set(key string, value interface{}) error {
	err := r.client.Set(context.Background(), key, value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}
