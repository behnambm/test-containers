package main

import (
	"fmt"
	"integration-test/cache"
	"log"
	"os"
)

type DataStore interface {
	Get(key string) (interface{}, error)
	Set(key string, value interface{}) error
}

func main() {
	// Example usage
	redisAddr := GetEnvOrPanic("REDIS_HOST")
	redisPassword := "" // no password for localhost
	redisDB := 0

	// Create a new RedisDataStore instance
	redisStore := cache.NewRedisDataStore(redisAddr, redisPassword, redisDB)

	// Set a key-value pair
	err := redisStore.Set("example_key", "example_value")
	if err != nil {
		fmt.Println("Error setting value:", err)
		return
	}

	val, err := redisStore.Get("example_key")
	if err != nil {
		fmt.Println("Error getting value:", err)
		return
	}
	fmt.Println("Retrieved value:", val)
}

func GetEnvOrPanic(name string) string {
	value := os.Getenv(name)
	if value == "" {
		log.Fatalf("cannot get %s from ENV", name)
	}

	return value
}
